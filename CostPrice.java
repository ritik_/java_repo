

public class CostPrice {
	private String name;                              
	private float weight;                               
	private int price;                              
	public CostPrice(){}             //constructor                          
	public CostPrice(String n){      //constructor
		name = n;
		weight = 100.5f;
		price = 50;
	}
	public CostPrice(String n, int price){      //constructor
		name = n;
		weight = 100.5f;
		this.price = price;
	}
	public float cprice()		//method
	{
		float cp = price - (float)(price * 0.1);
		return cp;
	}
	public String getS1(){return name;}                //method
	public float getS2(){return weight;}               //method
	public int getS3(){return price;}             	   //method
	//public float getS4(){return cp;}

	public static void main(String[] args) {					
		
		CostPrice items = new CostPrice("Glass",200);       	//object
		System.out.println("Name    = "+ items.getS1()); 		
		System.out.println("Weight  = "+ items.getS2());
		System.out.println("Price   = "+ items.getS3());
		System.out.println("Cost Price   =  " + items.cprice());
	}

}
